import torch
from stdpnet.tools import ConvLayerVisualizer


if __name__ == "__main__":
    net = torch.load("./train01/model.pth")
    conv_1 = net[0]
    conv_2 = net[2]
    conv_1_visualizer = ConvLayerVisualizer(conv_1)
    conv_2_visualizer = ConvLayerVisualizer(conv_2)
    conv_1_visualizer.update()
    conv_2_visualizer.update()
    _ = input("Press enter to exit")

import torch
import numpy as np

from sklearn.svm import SVC
from tonic.datasets import NMNIST
from typing import List, Iterable, Tuple
from tqdm import tqdm

from stdpnet.spike import Spike2D
from stdpnet.nn import STDPSequential, Conv2D, Pool2D
from stdpnet.tools import ConvLayerVisualizer

DEBUG = True


def extract_events(ds: NMNIST, event_count: int):
    return [(e[:event_count], label) for e, label in ds]


def event_to_spike_sample(events: np.ndarray) -> List[Spike2D]:
    return [
        Spike2D(x=event["x"], y=event["y"], channel=event["p"], time=event["t"])
        for event in events
    ]


def event_to_spike_dataset(dataset: NMNIST) -> List:
    dataset = tqdm(dataset)
    dataset.set_description("Converting events to spikes")
    return [(event_to_spike_sample(e), label) for e, label in dataset]


def shuffle_dataset(dataset: Iterable):
    shuffle_indice = torch.randperm(len(dataset))
    return [dataset[i] for i in shuffle_indice]


def build_dataset():
    print(f"Loading NMNIST dataset for training")
    train_ds = NMNIST(save_to="./data", train=True)
    train_ds = extract_events(train_ds, 600)
    train_ds = shuffle_dataset(train_ds)
    if DEBUG:
        train_ds = train_ds[:1000]
    train_ds = event_to_spike_dataset(train_ds)
    print(f"\n\nLoading NMNIST dataset for testing")
    test_ds = NMNIST(save_to="./data", train=False)
    test_ds = extract_events(test_ds, 600)
    test_ds = shuffle_dataset(test_ds)
    if DEBUG:
        test_ds = test_ds[:300]
    test_ds = event_to_spike_dataset(test_ds)
    return train_ds, test_ds


def accumulate_spike(spikes: List[Spike2D], channels: int):
    accumulation = np.zeros(channels)
    for s in spikes:
        accumulation[s.channel] += 1
    return accumulation


def convert_data_to_classifier_input(
    net: STDPSequential,
    train_ds: List[Tuple[List[Spike2D], int]],
    test_ds: List[Tuple[List[Spike2D], int]],
):
    ds = tqdm(train_ds)
    ds.set_description_str("Convert train dataset to network output")
    train_X = []
    train_Y = []
    for events, label in ds:
        spike_output = net(events)
        accumulation = accumulate_spike(spike_output, 32)
        train_X.append(accumulation)
        train_Y.append(label)
    train_X = np.array(train_X)
    train_Y = np.array(train_Y)

    ds = tqdm(test_ds)
    ds.set_description_str("Convert test dataset to network output")
    test_X = []
    test_Y = []
    for e, label in ds:
        spike_output = net(e)
        accumulation = accumulate_spike(spike_output, 32)
        test_X.append(accumulation)
        test_Y.append(label)
    test_X = np.array(test_X)
    test_Y = np.array(test_Y)
    return train_X, train_Y, test_X, test_Y


def evaluate(
    net: STDPSequential,
):
    train_ds, test_ds = build_dataset()
    pbar = tqdm(train_ds)
    pbar.set_description_str("Evaluating network")
    spike_count = []
    for events, label in pbar:
        spike_count_layer = np.zeros(len(net) + 1)
        spike_count_layer[0] = len(events)
        for i, layer in enumerate(net):
            events = layer(events)
            spike_count_layer[i + 1] = len(events)
        spike_count.append(spike_count_layer)
    spike_count = np.array(spike_count)
    print(spike_count.mean(0))


def main():
    net = torch.load("./records/train_0430/model.pth")
    for layer in net:
        if hasattr(layer, "train"):
            layer.train(False)
    evaluate(net=net)
    _ = input("<Press Enter to ESC>")


if __name__ == "__main__":
    main()

import torch
import numpy as np
import time
from typing import List
import matplotlib
import matplotlib.pyplot as plt

from matplotlib.figure import Figure
from matplotlib.axes import Axes

from ..nn import Conv2D

matplotlib.use("TkAgg")


class ConvLayerVisualizer:
    def __init__(self, conv: Conv2D, n_col: int = None, n_row: int = None):
        self.__conv = conv
        self.__init_check_whether_merge_channels()
        self.__init_n_col_row(n_col, n_row)
        self.__init_visualizer()

    def __init_n_col_row(self, n_col: int = None, n_row: int = None):
        if n_col is None and n_row is None:
            n_col = 5
            n_row = self.__conv.get_out_channels() // n_col + 1
        elif n_col is None:
            n_col = self.__conv.get_out_channels() // n_row + 1
        elif n_row is None:
            n_row = self.__conv.get_out_channels() // n_col + 1
        self.__n_row = n_row
        self.__n_col = n_col
        self.__total_channels = self.__conv.get_out_channels()

    def __init_visualizer(self):
        plt.ion()
        self.__fig, self.__axs = plt.subplots(
            ncols=self.__n_col, nrows=self.__n_row, figsize=(10, 10)
        )
        for i in range(self.__n_row):
            for j in range(self.__n_col):
                self.__axs[i, j].set_axis_off()
        # fill black background
        self.__fig.set_facecolor("black")

    def __init_check_whether_merge_channels(self):
        if self.__conv.get_in_channels() <= 3:
            self.__merge_channels = False
        else:
            self.__merge_channels = True

    def __img_padding(self, weight: np.ndarray):
        padding_dim = 3 - weight.shape[0]
        if padding_dim > 0:
            weight = np.concatenate(
                [weight, np.zeros((padding_dim, weight.shape[1], weight.shape[2]))],
                axis=0,
            )
        return weight

    def __gen_img(self, channel: int) -> np.ndarray:
        img = self.__conv.get_weight()[channel].cpu().numpy()
        if self.__merge_channels:
            img_sum_channels = img.sum(2).sum(1)
            img = img[img_sum_channels.argmax()]  # extract the highest weight channel
        else:
            img[img < 0] = 0
            img = img / np.max(img)
            img = self.__img_padding(img)
            img = np.transpose(img, (1, 2, 0))
        return img

    def update(self):
        for i in range(self.__total_channels):
            img = self.__gen_img(i)
            if self.__merge_channels:
                self.__axs[i // self.__n_col, i % self.__n_col].imshow(
                    img, cmap="plasma"
                )
            else:
                self.__axs[i // self.__n_col, i % self.__n_col].imshow(img)
        plt.draw()
        self.__fig.canvas.flush_events()
        self.__fig.show()

    def close(self):
        plt.close(self.__fig)

    def save(self, path: str):
        self.__fig.savefig(path)


class Conv2DWeightMonitor:
    def __init__(self, weight_path: str):

        self.__path = weight_path
        self.__init_visualizer()

    def __init_visualizer(self):
        self.__weight = torch.load(self.__path)
        self.__n_kernels = self.__weight.shape[0]
        self.__n_col = 5
        self.__n_row = self.__n_kernels // self.__n_col + 1
        self.__fig, self.__axs = plt.subplots(
            ncols=self.__n_col, nrows=self.__n_row, figsize=(8, 8)
        )
        for i in range(self.__n_row):
            for j in range(self.__n_col):
                self.__axs[i, j].set_axis_off()
        self.__fig.set_facecolor("black")

    def __img_padding(self, weight: np.ndarray):
        padding_dim = 3 - weight.shape[0]
        if padding_dim > 0:
            weight = np.concatenate(
                [weight, np.zeros((padding_dim, weight.shape[1], weight.shape[2]))],
                axis=0,
            )
        return weight

    def __gen_img(self, channel: int) -> np.ndarray:
        img: np.ndarray = self.__weight[channel].cpu().numpy()
        if img.shape[0] > 3:
            img_sum_channels = img.sum(2).sum(1)
            img = img[img_sum_channels.argmax()]  # extract the highest weight channel
            img = (img - img.min()) / (img.max() - img.min())

        else:
            img = (img - img.min()) / (img.max() - img.min())
            img = self.__img_padding(img)
            img = np.transpose(img, (1, 2, 0))

        return img

    def __update_weight(self):
        self.__weight = torch.load(self.__path)

    def update(self):
        self.__update_weight()
        for i in range(self.__n_kernels):
            img = self.__gen_img(i)
            self.__axs[i // self.__n_col, i % self.__n_col].imshow(img)
        plt.draw()
        self.__fig.canvas.flush_events()
        self.__fig.show()

    def close(self):
        plt.close(self.__fig)

    def run(self):
        while True:
            self.update()
            time.sleep(1)
            # save the figure
            file_path = self.__path.replace(".pth", ".png")
            self.__fig.savefig(file_path)

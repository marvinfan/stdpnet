import torch
from abc import abstractmethod
from .base import STDPModule
from typing import Union, Tuple, List


from ..spike import Spike2D, Spike


class NeuralMaps(STDPModule):
    def __init__(
        self,
        input_shape: Tuple[int, int, int],
        kernel_size: Union[int, Tuple[int, int]],
        stride: int,
        device: torch.device,
    ):
        self.__input_shape = input_shape
        self.__kernel_size = (
            (kernel_size, kernel_size) if isinstance(kernel_size, int) else kernel_size
        )
        self.__stride = (stride, stride) if isinstance(stride, int) else stride
        self.__device = device
        self.__neural_maps: torch.Tensor = self.init_neural_maps()

    @abstractmethod
    def init_neural_maps(self) -> torch.Tensor:
        pass

    def get_input_shape(self):
        return self.__input_shape

    def get_kernel_size(self):
        return self.__kernel_size

    def get_stride(self):
        return self.__stride

    def get_device(self) -> torch.device:
        return self.__device

    def get_neural_maps(self) -> torch.Tensor:
        return self.__neural_maps

    def to(self, device: torch.device) -> None:
        self.__neural_maps = self.__neural_maps.to(device=device)
        self.__device = self.__neural_maps.device


class PoolNeuralMaps(NeuralMaps):
    def __init__(
        self,
        input_shape: Tuple[int, int, int],
        kernel_size: Union[int, Tuple[int, int]],
        stride: int = 1,
        spike_threshold: float = 10.0,
        device: torch.device = "cpu",
    ):
        super().__init__(
            input_shape=input_shape,
            kernel_size=kernel_size,
            stride=stride,
            device=device,
        )
        self.__inhibit_maps = self.__init_inhibit_maps()
        self.__spike_threshold = spike_threshold

    def init_neural_maps(self) -> torch.Tensor:
        in_channels, in_h, in_w = self.get_input_shape()
        k_h, k_w = self.get_kernel_size()
        map_h = in_h - k_h + 1 + 2 * (k_h - 1)
        map_w = in_w - k_w + 1 + 2 * (k_w - 1)
        device = self.get_device()
        neural_maps = torch.zeros((in_channels, map_h, map_w), device=device)
        return neural_maps

    def __init_inhibit_maps(self):
        in_channels, in_h, in_w = self.get_input_shape()
        k_h, k_w = self.get_kernel_size()
        s_r, s_c = self.get_stride()
        map_h = in_h - k_h + 1 + 2 * (k_h - 1)
        map_w = in_w - k_w + 1 + 2 * (k_w - 1)
        out_h = in_h - k_h + 1
        out_w = in_w - k_w + 1
        device = self.get_device()
        inhibit_maps = torch.ones((in_channels, map_h, map_w), device=device)
        for i in range(in_channels):
            for r in range(map_h):
                for c in range(map_w):
                    if (
                        r < k_h - 1
                        or r >= out_h + (k_h - 1)
                        or c < k_w - 1
                        or c >= out_w + (k_w - 1)
                    ):
                        inhibit_maps[i, r, c] = 0
                        continue
                    if (r - (k_h - 1)) % s_r != 0:
                        inhibit_maps[i, r, c] = 0
                        continue
                    if (c - (k_w - 1)) % s_c != 0:
                        inhibit_maps[i, r, c] = 0
                        continue
        return inhibit_maps

    def forward(self, inp: Spike2D, *args, **kwargs) -> Union[None, Spike]:
        out = None
        k_h, k_w = self.get_kernel_size()
        s_r, s_c = self.get_stride()
        target_neural_map: torch.Tensor = self.get_neural_maps()[
            inp.channel, inp.y : inp.y + k_h, inp.x : inp.x + k_w
        ]
        inhbit_neural_map = self.__inhibit_maps[
            inp.channel, inp.y : inp.y + k_h, inp.x : inp.x + k_w
        ]
        target_neural_map += inhbit_neural_map
        if (target_neural_map >= self.__spike_threshold).any():
            argmax = target_neural_map.argmax()
            fired_neuron_row = argmax // k_w
            fired_neuron_col = argmax % k_w
            true_row = (fired_neuron_row + inp.y - (k_h - 1)) // s_r
            true_col = (fired_neuron_col + inp.x - (k_w - 1)) // s_c
            out = Spike2D(channel=inp.channel, x=true_col, y=true_row, time=inp.time)
            self.reset_neural_map(inp.channel)
        return out

    def reset_neural_map(self, channel: int):
        neural_maps = self.get_neural_maps()
        # _ = neural_maps[channel].zero_()
        _ = neural_maps.zero_()

    def reset_neural_maps(self):
        neural_maps = self.get_neural_maps()
        _ = neural_maps.zero_()

    def to(self, device: torch.device):
        super().to(device=device)
        self.__inhibit_maps = self.__inhibit_maps.to(device=device)

    def set_spike_threshold(self, v: float):
        self.__spike_threshold = v

    def get_spike_threshold(self):
        return self.__spike_threshold

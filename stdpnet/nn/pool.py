import torch
from .base import STDPModule
from typing import Union, Tuple, List


from ..spike import Spike2D, Spike
from .conv2d import Conv2D
from .neural_map import PoolNeuralMaps


class LocalPool2D(Conv2D):
    def __init__(
        self, input_shape: Union[int, Tuple], kernel_size: int, stride: int = None
    ):
        super().__init__(
            in_channels=input_shape[0],
            out_channels=input_shape[0],
            kernel_size=kernel_size,
            input_size=(input_shape[1], input_shape[2]),
            fire_threshold=1,
            stride=stride,
            train=False,
        )
        self.__init_weight()

    def __init_weight(self):
        channels, w, h = self.get_out_channels(), *self.get_kernel_size()
        self._rot_weight = torch.zeros((channels, channels, *self.get_kernel_size()))
        for i in range(self.get_out_channels()):
            self._rot_weight[i, i].fill_(1)

    def forward_spk(self, inp: Spike2D) -> Union[Spike2D, None]:
        out = super().forward_spk(inp)
        if out:
            self.reset_potential(out.channel)
        return out

    def train(self):
        return

    def eval(self):
        return


class Pool2D(STDPModule):
    def __init__(
        self,
        input_shape: Tuple[int, int, int],
        kernel_size: Union[int, Tuple[int, int]],
        stride: int,
        spike_threshold: int = 1,
        device: torch.device = "cpu",
    ):
        self.__input_shape = input_shape
        self.__kernel_size = (
            (kernel_size, kernel_size) if isinstance(kernel_size, int) else kernel_size
        )
        self.__stride = (stride, stride) if isinstance(stride, int) else stride
        self.__output_shape = self.__cal_output_shape()
        self.__spike_threshold = spike_threshold
        self.__device = device
        self.__init_neural_maps()

    def __cal_output_shape(self):
        out_h = (
            int((self.__input_shape[1] - self.__kernel_size[0]) / self.__stride[0]) + 1
        )
        out_w = (
            int((self.__input_shape[2] - self.__kernel_size[1]) / self.__stride[1]) + 1
        )
        return (self.__input_shape[0], out_h, out_w)

    def __init_neural_maps(self):
        self.__neural_maps = PoolNeuralMaps(
            input_shape=self.__input_shape,
            kernel_size=self.__kernel_size,
            stride=self.__stride,
            spike_threshold=self.__spike_threshold,
            device=self.__device,
        )

    def forward(self, inp: List[Spike2D], *args, **kwargs) -> List:
        return [
            out_spk for spk in inp if (out_spk := self.__neural_maps(spk)) is not None
        ]

    def set_spike_threshold(self, v: float):
        self.__neural_maps.set_spike_threshold(v=v)

    def get_spike_threshold(self):
        return self.__neural_maps.get_spike_threshold()

    def get_output_shape(self):
        return self.__output_shape

    def reset_potential(self):
        self.__neural_maps.reset_neural_maps()

    def to(self, device: torch.device):
        self.__neural_maps.to(device=device)


class GlobalPool(STDPModule):
    def __init__(self, n_channels: int = None, n_fire_threshold: int = 10):
        if n_channels:
            self.__n_channels = n_channels
        else:
            self.__n_channels = 10
        self.__accumulator = torch.zeros(self.__n_channels)
        self.__n_fire_threshold = n_fire_threshold

    def forward_spk(self, inp: Spike) -> Union[None, Spike]:
        if inp.channel >= self.__n_channels:
            self.__accumulator = torch.cat(
                (self.__accumulator, torch.zeros(inp.channel - self.__n_channels))
            )
            self.__n_channels = inp.channel + 1
        self.__accumulator[inp.channel] += 1
        if self.__accumulator[inp.channel] >= self.__n_fire_threshold:
            self.__accumulator.fill_(0)
            return Spike(channel=inp.channel, time=inp.time)

    def forward(self, inp: List[Spike]) -> List[Spike]:
        return [
            out_spk for spk in inp if (out_spk := self.forward_spk(spk)) is not None
        ]

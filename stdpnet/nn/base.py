from abc import ABC, abstractmethod
from ..spike import Spike

from typing import Any, Union, List


class NeuronMaps(ABC):
    """Abstract class for neuron maps."""

    def __init__(self, *size: tuple):
        self._size = size

    @abstractmethod
    def forward(self, *args, **kwargs) -> Spike:
        pass

    @abstractmethod
    def get_input_shape(self) -> tuple:
        pass

    @abstractmethod
    def get_output_shape(self) -> tuple:
        pass

    def __call__(self, *args, **kwargs):
        return self.forward(*args, **kwargs)


class STDPModule(ABC):

    @abstractmethod
    def forward(self, inp: List[Spike], *args, **kwargs) -> List[Spike]:
        pass

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return self.forward(*args, **kwds)

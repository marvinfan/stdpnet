from ..spike import Spike2D, Spike
from .base import STDPModule

from typing import List, Tuple


class Flatten(STDPModule):
    def __init__(self, input_shape: Tuple[int, int, int]) -> None:
        self.__input_shape = input_shape
        self.__out_channels = input_shape[0] * input_shape[1] * input_shape[2]

    def __forward_spk(self, spk: Spike2D) -> Spike:
        h, w = self.__input_shape[1:]
        channel = spk.channel * h * w + spk.y * w + spk.x
        return Spike(channel=channel, time=spk.time)

    def forward(self, inp: List[Spike]) -> List[Spike]:
        return [self.__forward_spk(spk) for spk in inp]

    def get_input_shape(self):
        return self.__input_shape

    def get_out_channels(self):
        return self.__out_channels

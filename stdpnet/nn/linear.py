import torch
import numpy as np

from abc import ABC, abstractmethod
from ..spike import Spike
from .base import STDPModule

from typing import List, Tuple, Union


class LinearNeuralMaps(ABC):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        spike_threshold: float = 1,
        min_v_mem: float = 0,
    ):
        self.__in_channels = in_channels
        self.__out_channels = out_channels
        self.__spike_threshold = spike_threshold
        self.__min_v_mem = min_v_mem
        self.__init_neural_maps()

    def __init_neural_maps(self):
        self.__neural_maps = torch.zeros((self.__in_channels, self.__out_channels))
        self.__neural_maps.fill_(self.__min_v_mem)

    @abstractmethod
    def forward(self, *args, **kwargs) -> Spike:
        pass

    def __call__(self, *args: torch.Any, **kwds: torch.Any) -> torch.Any:
        return self.forward(args, kwargs=kwds)

    def get_in_channels(self) -> int:
        return self.__in_channels

    def get_out_channels(self) -> int:
        return self.__out_channels

    def get_neural_maps(self) -> int:
        return self.__neural_maps

    def get_spike_threshold(self) -> int:
        return self.__spike_threshold

    def get_min_v_mem(self) -> int:
        return self.__min_v_mem

    def set_spike_threshold(self, v: float) -> None:
        self.__spike_threshold = v

    def set_min_v_mem(self, v: float) -> None:
        self.__min_v_mem = v

    def reset_potential(self) -> None:
        self.__init_neural_maps()


class NeuralMapsIAF(LinearNeuralMaps):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        spike_threshold: float = 1,
        min_v_mem: float = 0,
    ):
        super().__init__(in_channels, out_channels, spike_threshold, min_v_mem)

    def forward(
        self, synapse: torch.Tensor, in_channel: int, time: int = 0
    ) -> Union[None, Spike]:
        neural_maps = self.get_neural_maps()
        target_neurons: torch.Tensor = neural_maps[in_channel]
        target_neurons += synapse
        low_mask = target_neurons < self.get_min_v_mem()
        target_neurons.masked_fill_(low_mask, self.get_min_v_mem())
        reached_threshold = target_neurons >= self.get_spike_threshold()
        if reached_threshold.any():
            spike_neuron_idx = torch.argmax(target_neurons)
            target_neurons[reached_threshold].fill_(self.get_min_v_mem())
            return Spike(channel=spike_neuron_idx, time=time)


class Linear(STDPModule):
    def __init__(
        self,
        in_channels,
        out_channels,
        spike_threshold: float = 1,
        lr_potentiation: float = 1 * 1e-4,
        lr_depression: float = 1 * 1e-4,
        max_weight: float = 1.0,
        min_weight: float = -1.0,
        min_v_mem: float = 0.0,
        criterion: float = 0.0,
        decay_rate: float = 1.0,
    ):
        self.__in_channels = in_channels
        self.__out_channels = out_channels
        self.__spike_threshold = spike_threshold
        self.__lr_potentiation: float = lr_potentiation
        self.__lr_depression: float = lr_depression
        self.__min_v_mem = min_v_mem
        self.__spk_order = 0
        self.__train = False
        self.__max_weight = max_weight
        self.__min_weight = min_weight
        self.__max_order = 2**32 - 1
        self.__criterion = criterion
        self.__decay_rate = decay_rate
        self.__init_maps()
        self.__init_weights()

    def __init_maps(self):
        self.__neural_maps = NeuralMapsIAF(
            self.__in_channels,
            self.__out_channels,
            self.__spike_threshold,
            self.__min_v_mem,
        )
        self.__presynapse_trace_maps = torch.zeros(self.__in_channels)
        self.__postsynapse_trace_maps = torch.zeros(self.__out_channels)

    def __init_weights(self):
        weight = np.random.normal(
            loc=0.7,
            scale=0.05,
            size=(self.__in_channels, self.__out_channels),
        )
        self.__weight = torch.tensor(weight, dtype=torch.float32)

    def cal_convergence(self):
        weight = self.__weight
        return ((self.__max_weight - weight) * (weight - self.__min_weight)).mean()

    def is_converged(self):
        return self.cal_convergence() < self.__criterion

    def update_spk_order(self):
        self.__spk_order += 1
        if self.__spk_order > self.__max_order:
            self.__presynapse_trace_maps.fill_(0)
            self.__postsynapse_trace_maps.fill_(0)
            self.__spk_order = 0

    def stdp(self, out_channel: int):
        post_trace = self.__postsynapse_trace_maps[out_channel]
        stdp_mask = torch.where(
            self.__presynapse_trace_maps > post_trace,
            self.__lr_potentiation,
            -self.__lr_depression,
        )
        update_weight = self.__weight[:, out_channel]
        update_weight.mul_(self.__decay_rate)
        update_weight += (
            (self.__max_weight - update_weight)
            * (update_weight - self.__min_weight)
            * stdp_mask
        )
        pass

    def forward(self, spks: List[Spike]):
        out = []
        for s in spks:
            self.update_spk_order()
            s.time = self.__spk_order
            self.__presynapse_trace_maps[s.channel] = self.__spk_order
            synapse = self.__weight[s.channel]
            out_spk = self.__neural_maps.forward(
                synapse=synapse, in_channel=s.channel, time=s.time
            )
            if out_spk:
                out.append(out_spk)
                if self.__train:
                    self.stdp(out_spk.channel)
                self.__postsynapse_trace_maps[out_spk.channel] = self.__spk_order
        return out

    def train(self, arg: bool = True):
        self.__train = arg

    def get_weight(self):
        return torch.transpose(self.__weight, 0, 1)

    def get_output_channels(self):
        return self.__out_channels

    def get_output_shape(self):
        return self.__out_channels

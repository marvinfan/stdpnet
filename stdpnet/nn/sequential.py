import torch

from .base import STDPModule


class STDPSequential(STDPModule):
    def __init__(self, *args) -> None:
        self.modules = [*args]

    def __len__(self):
        return len(self.modules)

    def __getitem__(self, index):
        return self.modules[index]

    def forward(self, inp):
        for module in self.modules:
            inp = module(inp)
        return inp

    def get_input_shape(self):
        return self.modules[0].get_input_shape()

    def get_output_shape(self):
        return self.modules[-1].get_output_shape()

    def get_weights(self):
        return [
            module.get_weights()
            for module in self.modules
            if hasattr(module, "get_weights")
        ]

    def __str__(self):
        out = "STDPSequential(\n"
        for i, module in enumerate(self.modules):
            out += f"    ({i}): {module}\n"
        out += ")"
        return out

    def save_weights(self, path):
        weights = self.get_weights()
        torch.save(weights, path)

    def save(self, path):
        torch.save(self, path)

import time
import torch
import numpy as np
from typing import TYPE_CHECKING, Any, Iterable, List, Tuple, Union
from collections import deque

from ..spike import Spike2D


def timer(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        out = func(*args, **kwargs)
        end = time.time()
        print(f"{func.__name__} takes {(end-start):0.6} seconds")
        return out

    return wrapper


class PresynapticalTrace:
    def __init__(
        self,
        input_shape: tuple,
        kernel_size: tuple,
        device: torch.device = torch.device("cpu"),
    ):
        self.__input_shape = input_shape
        self.__kernel_size = kernel_size
        self.__device = device
        self.__relative_window = self.__cal_relative_window()
        self.__init_map()

    def __cal_relative_window(self) -> int:
        u, v = self.__kernel_size
        in_channels = self.__input_shape[0]
        return u * v * in_channels

    def __init_map(self):
        self.__map = torch.zeros(
            self.__input_shape, device=self.__device, dtype=torch.int64
        )
        self.__map.fill_(-1)

    def reset(self):
        self.__map.fill_(-1)

    def forward(self, inp: Spike2D) -> None:
        inp_time = inp.time
        self.__map[inp.channel, inp.y, inp.x] = inp_time

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return self.forward(*args, **kwds)

    def get_stdp_mask(self, output_spk: Spike2D, last_fired_time: int) -> torch.Tensor:
        target_map = self.__map[
            :,
            output_spk.y : output_spk.y + self.__kernel_size[0],
            output_spk.x : output_spk.x + self.__kernel_size[1],
        ]
        out = target_map > last_fired_time
        out = out.flip(dims=(1, 2))
        return out


class PostsynapticalTrace:
    def __init__(self, output_shape: Tuple[int, int, int]):
        self.__output_shape = output_shape
        self.__init_map()

    def __init_map(self):
        # self.__map = torch.zeros(self.__output_shape, dtype=torch.int64)
        self.__map = torch.zeros(self.__output_shape[0], dtype=torch.int64)

    def reset(self):
        self.__map.fill_(0)

    def forward(self, output_spk: Spike2D) -> None:
        self.__map[output_spk.channel] = output_spk.time

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return self.forward(*args, **kwds)

    def get_trace(self, output_spk: Spike2D) -> int:
        return self.__map[output_spk.channel]


class SubNeuralMaps:
    def __init__(
        self,
        sub_neural_map: torch.Tensor,
        sub_inhibit_mask: torch.Tensor,
        coordinates: Tuple[int, int],
        spike_threshold: float,
        min_v_mem: float = 0.0,
    ):
        self.__neural_map = sub_neural_map
        self.__inhibit_mask = sub_inhibit_mask
        self.__coordinates = coordinates
        self.__spike_threshold = spike_threshold
        self.__height = sub_neural_map.shape[1]
        self.__width = sub_neural_map.shape[2]
        self.__min_v_mem = min_v_mem

    def convert_flatten_arg_to_3d(self, idx: int) -> Tuple[int, int, int]:
        channel = idx // (self.__height * self.__width)
        y = (idx % (self.__height * self.__width)) // self.__width
        x = idx % self.__width
        return channel, y, x

    def forward(self, stimulation: torch.Tensor) -> Union[Tuple[int, int, int], None]:
        self.__neural_map += stimulation * self.__inhibit_mask
        low_mask = self.__neural_map < self.__min_v_mem
        self.__neural_map.masked_fill_(low_mask, self.__min_v_mem)
        fire_mask = self.__neural_map >= self.__spike_threshold
        if fire_mask.any():
            fired_idx = torch.argmax(
                self.__neural_map
            )  # only fire the neuron with the highest potential
            channel = int(fired_idx // (self.__height * self.__width))
            fired_y_local = (fired_idx % (self.__height * self.__width)) // self.__width
            fired_x_local = fired_idx % self.__width
            fired_y_global = int(fired_y_local + self.__coordinates[0])
            fired_x_global = int(fired_x_local + self.__coordinates[1])
            self.__neural_map.masked_fill_(fire_mask, self.__min_v_mem)
            return channel, fired_y_global, fired_x_global

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        return self.forward(*args, **kwds)


class WTACache:
    def __init__(
        self,
        neural_map: torch.Tensor,
        radius: Tuple[int, int] = (1, 1),
        min_v_mem: float = 0.0,
        stride: Tuple[int, int] = (1, 1),
    ):
        self.__neural_map = neural_map
        self.__radius = radius
        self.__min_v_mem = min_v_mem
        self.__stride = stride
        self.__init_cache()

    def __init_cache(self):
        self.__cache = {}
        for i in range(self.__neural_map.shape[1]):
            for j in range(self.__neural_map.shape[2]):
                self.__cache[(i, j)] = self.__get_neighbors(i, j)

    def __get_neighbors(self, y: int, x: int):
        y_range = (
            y - self.__radius[0] * self.__stride[0],
            y + self.__radius[0] * self.__stride[0] + 1,
        )
        x_range = (
            x - self.__radius[1] * self.__stride[1],
            x + self.__radius[1] * self.__stride[1] + 1,
        )
        y_range = (max(0, y_range[0]), min(self.__neural_map.shape[1], y_range[1]))
        x_range = (max(0, x_range[0]), min(self.__neural_map.shape[2], x_range[1]))
        return self.__neural_map[:, y_range[0] : y_range[1], x_range[0] : x_range[1]]

    def __inhibit_neighbors(self, y: int, x: int):
        neighbors: torch.Tensor = self.__cache[(y, x)]
        neighbors.fill_(self.__min_v_mem)

    def forward(self, y: int, x: int) -> None:
        self.__inhibit_neighbors(y, x)

    def __call__(self, *args: Any, **kwds: Any) -> None:
        self.forward(*args, **kwds)


class RotWeightCache:
    def __init__(self, rot_weight: torch.Tensor):
        self.__rot_weight = rot_weight
        self.__init_cache()

    def __init_cache(self):
        self.__cache = {}
        for i in range(self.__rot_weight.shape[1]):
            self.__cache[i] = self.__rot_weight[:, i]

    def forward(self, spk: Spike2D) -> torch.Tensor:
        return self.__cache[spk.channel]

    def __call__(self, *args: Any, **kwds: Any) -> torch.Tensor:
        return self.forward(*args, **kwds)


class NeuralMapCoordinateConverter:
    def __init__(
        self,
        neural_map_shape: Tuple[int, int, int],
        kernel_size: Tuple[int, int],
        stride: Tuple[int, int],
    ):
        self.__neural_map_shape = neural_map_shape
        self.__kernel_size = kernel_size
        self.__stride = stride
        self.__init_map()

    def __init_map(self):
        self.__map = np.zeros(
            (self.__neural_map_shape[1], self.__neural_map_shape[2], 2), dtype=int
        )
        for i in range(self.__neural_map_shape[1]):
            for j in range(self.__neural_map_shape[2]):
                real_y = (i - (self.__kernel_size[0] - 1)) // self.__stride[0]
                real_x = (j - (self.__kernel_size[1] - 1)) // self.__stride[1]
                self.__map[i, j] = (real_y, real_x)
        return

    def forward(self, y: int, x: int) -> Tuple[int, int]:
        return self.__map[y][x]

    def __call__(self, y: int, x: int) -> Tuple[int, int]:
        return self.__map[y][x]


class Conv2D:
    def __init__(
        self,
        input_shape: Tuple[int, int, int],
        out_channels: int,
        kernel_size: Tuple[int, int],
        stride: Union[int, Iterable[int]] = 1,
        min_v_mem: float = 0.0,
        spike_threshold: float = 1.0,
        lr_potentiation: float = 1e-4,
        lr_depression: float = 1e-4,
        train: bool = False,
        convergence_criteria: float = 1e-3,
        device: torch.device = torch.device("cpu"),
        min_weight: float = -0.1,
        max_weight: float = 1.0,
        r_wta: Union[int, Tuple[int, int]] = None,
        decay_rate: float = 1 - 1e-3,
    ):
        self.__input_shape = input_shape
        self.__out_channels = out_channels
        self.__kernel_size = (
            kernel_size
            if isinstance(kernel_size, Iterable)
            else (
                kernel_size,
                kernel_size,
            )
        )
        self.__stride = stride if isinstance(stride, Iterable) else (stride, stride)
        self.__min_v_mem = min_v_mem
        self.__spike_threshold = spike_threshold
        self.__lr_potentiation = lr_potentiation
        self.__lr_depression = lr_depression
        self.__train = train
        self.__convergence_criteria = convergence_criteria
        self.__device = device
        self.__min_weight = min_weight
        self.__max_weight = max_weight
        self.__decay_rate = decay_rate
        self.__order = 0
        if r_wta:
            self.__r_wta = r_wta if isinstance(r_wta, Iterable) else (r_wta, r_wta)
        else:
            self.__r_wta = (self.__kernel_size[0] // 2, self.__kernel_size[1] // 2)

        self.__max_order = 2**32 - 1
        self.__init_params()
        self.__init_cache_maps()

    def __init_neural_maps(self):
        """
        Make a neuron map for each output channel.

        To increase the speed of the forward pass, the neuron maps are padded.
        """

        width, height = (
            self.__input_shape[2] - self.__kernel_size[1] + 1,
            self.__input_shape[1] - self.__kernel_size[0] + 1,
        )
        padded_width = width + (self.__kernel_size[0] - 1) * 2
        padded_height = height + (self.__kernel_size[1] - 1) * 2
        self.__raw_neural_maps = torch.rand(
            self.__out_channels, padded_width, padded_height
        )
        # # make the padded area of the neuron maps to be -inf which will not fire spikes
        self.__raw_counter_map = self.__raw_neural_maps.to(self.__device)

    def __init_weight(self):
        # weight = np.random.uniform(
        #     low=0.9 / (self.__kernel_size[0] * self.__kernel_size[1] * 0.5),
        #     high=1 / (self.__kernel_size[0] * self.__kernel_size[1] * 0.5),
        #     size=(self.__out_channels, self.__in_channels, *self.__kernel_size),
        # )
        weight = np.random.normal(
            loc=0.8,
            scale=0.03,
            size=(self.__out_channels, self.__input_shape[0], *self.__kernel_size),
        )
        self._rot_weight = torch.tensor(
            weight, device=self.__device, dtype=torch.float32
        )

    def __init_presynaptical_trace(self):
        self.__presynaptical_trace = PresynapticalTrace(
            input_shape=self.__input_shape,
            kernel_size=self.__kernel_size,
            device=self.__device,
        )

    def __init_postsynaptical_trace(self):
        self.__postsyanptical_trace = PostsynapticalTrace(
            output_shape=(self.__out_channels, *self.get_output_size())
        )

    def __init_inhibit_mask(self):
        self.__inhibit_mask = torch.ones_like(self.__raw_neural_maps, dtype=torch.bool)
        mask_shape = self.__inhibit_mask.shape
        for i in range(mask_shape[1]):
            for j in range(mask_shape[2]):
                # padding area
                if (
                    i < self.__kernel_size[0] - 1
                    or i >= mask_shape[1] - self.__kernel_size[0] + 1
                ):
                    self.__inhibit_mask[:, i, j] = False
                    continue
                if (
                    j < self.__kernel_size[1] - 1
                    or j >= mask_shape[2] - self.__kernel_size[1] + 1
                ):
                    self.__inhibit_mask[:, i, j] = False
                    continue
                # stride area
                if (i - (self.__kernel_size[0] - 1)) % self.__stride[0] != 0:
                    self.__inhibit_mask[:, i, j] = False
                    continue
                if (j - (self.__kernel_size[1] - 1)) % self.__stride[1] != 0:
                    self.__inhibit_mask[:, i, j] = False
                    continue

    def __init_sub_neural_maps(self):
        self.__sub_neural_maps = []
        input_shape = self.get_input_shape()
        for y in range(input_shape[1]):
            self.__sub_neural_maps.append([])
            for x in range(input_shape[2]):
                sub_neural_map = self.__raw_neural_maps[
                    :,
                    y : y + self.__kernel_size[0],
                    x : x + self.__kernel_size[1],
                ]
                sub_inhibit_mask = self.__inhibit_mask[
                    :,
                    y : y + self.__kernel_size[0],
                    x : x + self.__kernel_size[1],
                ]
                self.__sub_neural_maps[y].append(
                    SubNeuralMaps(
                        sub_neural_map=sub_neural_map,
                        sub_inhibit_mask=sub_inhibit_mask,
                        coordinates=(y, x),
                        spike_threshold=self.__spike_threshold,
                        min_v_mem=self.__min_v_mem,
                    )
                )

    def __init_rot_weight_cache(self):
        self.__rot_weight_cache = RotWeightCache(self._rot_weight)

    def __init_params(self):
        self.__init_neural_maps()
        self.__init_weight()
        self.__init_presynaptical_trace()
        self.__init_postsynaptical_trace()

    def __init_coordinate_converter(self):
        self.__neural_map_coordinate_converter = NeuralMapCoordinateConverter(
            neural_map_shape=self.__raw_neural_maps.shape,
            kernel_size=self.__kernel_size,
            stride=self.__stride,
        )

    def __init_wta_cache(self):
        self.__wta_cache = WTACache(
            neural_map=self.__raw_neural_maps,
            radius=self.__r_wta,
            min_v_mem=self.__min_v_mem,
            stride=self.__stride,
        )

    def __init_cache_maps(self):
        self.__init_inhibit_mask()
        self.__init_sub_neural_maps()
        self.__init_rot_weight_cache()
        self.__init_coordinate_converter()
        self.__init_wta_cache()

    def __forward_iaf(self, inp: Spike2D) -> Spike2D:
        """IAF neurons forward pass.

        Process the input 2D spike and return the output 2D spikes if any.

        Args:
            inp (Spike2D): The input 2D spike.

        Returns:
            List[Spike2D]: The output 2D spikes.
        """
        out = None
        sub_neural_map = self.__sub_neural_maps[inp.y][inp.x]
        stimulation = self.__rot_weight_cache(inp)
        fired_neuron = sub_neural_map(stimulation)
        if fired_neuron is not None:
            channel, fired_y_global, fired_x_global = fired_neuron
            self.__wta_cache(fired_y_global, fired_x_global)
            fired_y, fired_x = self.__neural_map_coordinate_converter(
                fired_y_global, fired_x_global
            )
            out = Spike2D(channel=channel, y=fired_y, x=fired_x, time=inp.time)
        return out

    def __cal_weight_delta(self, weight: torch.Tensor) -> torch.Tensor:
        """Calculate the weight delta.

        Args:
            weight (torch.Tensor): The weight.

        Returns:
            torch.Tensor: The weight delta.
        """
        return (self.__max_weight - weight) * (weight - (self.__min_weight))

    def __stdp(self, output_spk: Spike2D) -> None:
        """Update the weight according to the output spike.

        Args:
            output_spk (Spike2D): The output spike.
        """
        self._rot_weight[output_spk.channel].mul_(
            self.__decay_rate
        )  # weight decay, ensure neurons alive
        weight_delta = self.__cal_weight_delta(self._rot_weight[output_spk.channel])
        # stdp_map = self.__gen_stdp_map(output_skp=output_spk)
        stdp_mask = self.__presynaptical_trace.get_stdp_mask(
            output_spk=output_spk,
            last_fired_time=self.__postsyanptical_trace.get_trace(output_spk),
        )
        delta_weight = torch.where(
            stdp_mask,
            weight_delta * self.__lr_potentiation,
            -weight_delta * self.__lr_depression,
        )
        self._rot_weight[output_spk.channel] += delta_weight

    def reset_potential(self, channel: int = None):
        if channel is not None:
            target = self.__raw_neural_maps[channel]
            mask = self.__inhibit_mask[channel]
        else:
            target = self.__raw_neural_maps
            mask = self.__inhibit_mask
        _ = target.masked_fill_(mask=mask, value=self.__min_v_mem)

    def reset_trace(self):
        self.__presynaptical_trace.reset()
        self.__postsyanptical_trace.reset()

    def forward_spk(self, inp: Spike2D) -> Union[Spike2D, None]:
        """Forward the input spike.

        Args:
            inp (Spike2D): The input spike.

        Returns:
            List[Spike2D]: The output spikes.
        """
        self.__order += 1
        if self.__order > self.__max_order:
            self.__order = 0
            self.reset_potential()
            self.reset_trace()
        inp.time = self.__order
        out = self.__forward_iaf(inp)
        if self.__train:
            self.__presynaptical_trace(inp)
            if out is not None:
                self.__stdp(out)
                self.__postsyanptical_trace(out)
        return out

    def __call__(self, args: List[Spike2D]) -> List[Spike2D]:
        return self.forward(args)

    def forward(self, args: List[Spike2D]) -> List[Spike2D]:
        """Forward the input spikes.

        Args:
            args (List[Spike2D]): The input spikes.

        Returns:
            List[Spike2D]: The output spikes.
        """
        return [y for inp in args if (y := self.forward_spk(inp)) is not None]

    def train(self, arg: bool = True):
        self.__train = arg

    def cal_convergence(self) -> float:
        return self.__cal_weight_delta(self._rot_weight).mean()

    def is_converged(self) -> bool:
        return self.cal_convergence() < self.__convergence_criteria

    def to(self, device: torch.device):
        self.__device = device
        self.__raw_neural_maps = self.__raw_neural_maps.to(device)
        self.__raw_counter_map = self.__raw_counter_map.to(device)
        self._rot_weight = self._rot_weight.to(device)

    # Getters and Setters

    def get_input_size(self):
        return self.__input_shape[1:]

    def get_input_shape(self):
        return self.__input_shape

    def get_output_size(self):
        return (
            (self.__input_shape[1] - self.__kernel_size[0]) // self.__stride[0] + 1,
            (self.__input_shape[2] - self.__kernel_size[1]) // self.__stride[1] + 1,
        )

    def get_output_shape(self):
        return (self.__out_channels, *self.get_output_size())

    def get_weight(self):
        out = self._rot_weight
        return torch.flip(out, dims=(2, 3))

    def get_neuron_map(self):
        return self.__raw_neural_maps[
            :,
            self.__kernel_size[0] - 1 : -self.__kernel_size[0] + 1,
            self.__kernel_size[1] - 1 : -self.__kernel_size[1] + 1,
        ]

    def set_weight(self, weight: torch.Tensor) -> None:
        assert weight.shape == (
            self.__out_channels,
            self.__input_shape[0],
            *self.__kernel_size,
        ), "weight shape mismatch"
        self._rot_weight = weight.clone().to(self.__device).flip(dims=(2, 3))

    def get_out_channels(self):
        return self.__out_channels

    def get_in_channels(self):
        return self.__input_shape[0]

    def get_kernel_size(self):
        return self.__kernel_size

    def get_spike_threshold(self):
        return self.__spike_threshold

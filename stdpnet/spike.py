class Spike:
    def __init__(self, channel, time):
        self.channel = channel
        self.time = time

    def __str__(self):
        return f"Spike({self.channel}, {self.time})"


class Spike2D(Spike):
    def __init__(self, x, y, channel, time):
        super().__init__(channel, time)
        self.x = x
        self.y = y

    def __str__(self):
        return f"Spike 2D({self.x}, {self.y}, {self.channel}, {self.time})"

import numpy as np
import time


if __name__ == "__main__":
    st = time.time()
    for _ in range(100):
        img = np.zeros((2, 128, 128))
        for i in range(300):
            img[0, i % 128, i % 128] = 1
    print(time.time() - st)

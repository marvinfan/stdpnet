from stdpnet.nn import Conv2D
from stdpnet.spike import Spike2D
import numpy as np
import time

if __name__ == "__main__":
    out_channel = 3
    conv = Conv2D(
        in_channels=1,
        input_size=128,
        out_channels=out_channel,
        kernel_size=3,
        fire_threshold=10,
        lr_potentiation=0.004,
        lr_depression=0.002,
        train=True,
    )
    init_weight = conv.get_weight()
    np.set_printoptions(precision=2)
    for j in range(200):
        try:
            print(f"Iter:{j+1}\n")
            first_spks = [
                Spike2D(
                    x=(i) % 128,
                    y=(i) % 128,
                    channel=0,
                    time=None,
                )
                for i in range(1000)
            ]
            st = time.time()
            out = conv(first_spks)
            # print(f"elapsed time: {time.time() - st}")
            out_count = np.zeros(out_channel)
            for spk in out:
                out_count[spk.channel] += 1
            if out_count.sum() > 0:
                print(f"first sample output rate\n{out_count/out_count.sum()}\n")
            else:
                print(f"first sample output rate\n{[0, 0, 0]}")
            second_spks = [
                Spike2D(
                    x=127 - (i % 128),
                    y=(i) % 128,
                    channel=0,
                    time=None,
                )
                for i in range(1000)
            ]
            st = time.time()
            out = conv(second_spks)
            # print(f"elapsed time: {time.time() - st}")
            out_count = np.zeros(out_channel)
            for spk in out:
                out_count[spk.channel] += 1
            if out_count.sum() > 0:
                print(f"second sample output rate\n{out_count/out_count.sum()}\n")
            else:
                print(f"second sample output rate\n{[0, 0, 0]}")
            third_spks = [
                Spike2D(
                    x=i % 128,
                    y=63,
                    channel=0,
                    time=None,
                )
                for i in range(1000)
            ]
            st = time.time()
            out = conv(third_spks)
            # print(f"elapsed time: {time.time() - st}")
            out_count = np.zeros(out_channel)
            for spk in out:
                out_count[spk.channel] += 1
            if out_count.sum() > 0:
                print(f"third sample output rate\n{out_count/out_count.sum()}\n")
            else:
                print(f"third sample output rate\n{[0, 0, 0]}")
            continue
        except KeyboardInterrupt:
            break
    post_weight = conv.get_weight()
    print(f"Post weight\n{post_weight}")
    weight_delta = post_weight - init_weight
    print(f"Weight delta\n{weight_delta[:,0,:,:]}")
    print(f"Vmem\n{conv.get_neuron_map()}")

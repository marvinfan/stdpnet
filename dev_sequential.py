from spkconv.nn import STDPConv2D, STDPSequential, EventAvgPool2DKernel
from spkconv.events import Spike2D
import numpy as np
import time


if __name__ == "__main__":
    out_channel = 3
    conv_1 = STDPConv2D(
        in_channels=1,
        input_size=128,
        out_channels=3,
        kernel_size=3,
        lr_potentiation=0.02,
        lr_depression=0.003,
        device="cpu",
    )
    pool_1 = EventAvgPool2DKernel(
        input_shape=conv_1.get_output_shape(),
        kernel_size=2,
        stride=2,
    )
    conv_2 = STDPConv2D(
        in_channels=3,
        input_size=pool_1.get_output_shape()[1:],
        out_channels=3,
        kernel_size=5,
        lr_potentiation=0.004,
        lr_depression=0.003,
    )
    pool_2 = EventAvgPool2DKernel(
        input_shape=conv_2.get_output_shape(),
        kernel_size=2,
        stride=2,
    )
    # layer_3 = STDPConv2D(
    #     in_channels=3,
    #     input_size=layer_2.get_output_shape()[1:],
    #     out_channels=3,
    #     kernel_size=3,
    #     lr_potentiation=0.1,
    #     lr_depression=0.005,
    # )

    network = STDPSequential(conv_1)
    print(network)
    np.set_printoptions(precision=2)
    conv_1.start_training()
    for j in range(200):
        try:
            print(f"Iter:{j+1}\n")
            first_spks = [
                Spike2D(
                    x=(i) % 128,
                    y=(i) % 128,
                    channel=0,
                    time=None,
                )
                for i in range(1000)
            ]
            st = time.time()
            out = network(first_spks)
            # print(f"elapsed time: {time.time() - st}")
            out_count = np.zeros(out_channel)
            for spk in out:
                out_count[spk.channel] += 1
            if out_count.sum() > 0:
                print(f"first sample output rate\n{out_count/out_count.sum()}\n")
            else:
                print(f"first sample output rate\n{[0, 0, 0]}")
            second_spks = [
                Spike2D(
                    x=127 - (i % 128),
                    y=(i) % 128,
                    channel=0,
                    time=None,
                )
                for i in range(1000)
            ]
            st = time.time()
            out = network(second_spks)
            # print(f"elapsed time: {time.time() - st}")
            out_count = np.zeros(out_channel)
            for spk in out:
                out_count[spk.channel] += 1
            if out_count.sum() > 0:
                print(f"second sample output rate\n{out_count/out_count.sum()}\n")
            else:
                print(f"second sample output rate\n{[0, 0, 0]}")
            third_spks = [
                Spike2D(
                    x=i % 128,
                    y=63,
                    channel=0,
                    time=None,
                )
                for i in range(1000)
            ]
            st = time.time()
            out = network(third_spks)
            # print(f"elapsed time: {time.time() - st}")
            out_count = np.zeros(out_channel)
            for spk in out:
                out_count[spk.channel] += 1
            if out_count.sum() > 0:
                print(f"third sample output rate\n{out_count/out_count.sum()}\n")
            else:
                print(f"third sample output rate\n{[0, 0, 0]}")
            print(conv_1.get_converge_score())
            if conv_1.is_converged():
                conv_1.stop_training()
                break
        except KeyboardInterrupt:
            break

    print(network[0].get_weight())
    # print(network[1].get_weight())

from spkconv.nn import EventAvgPool2DKernel
from spkconv.events import Spike2D


if __name__ == "__main__":
    layer = EventAvgPool2DKernel(
        input_shape=(2, 128, 128), kernel_size=(3, 3), stride=1
    )
    spks = [Spike2D(x=2, y=2, channel=0, time=None)] * 9
    out = layer(spks)
    for e in out:
        print(e)

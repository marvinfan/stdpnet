import numpy as np
import torch
import os
import yaml

from torch.utils.data import DataLoader
from multiprocessing import Process
from sklearn.svm import SVC

from stdpnet.nn import Conv2D, Pool2D, STDPSequential, Pool2D, Flatten, Linear
from stdpnet.spike import Spike2D

from stdpnet.tools import Conv2DWeightMonitor
from tonic.datasets import NMNIST
from tqdm import tqdm
from typing import List, Tuple, Iterable

# # Set environment variables for MKL
# os.environ["MKL_ENABLE_INSTRUCTIONS"] = "SSE4_2"


DEBUG = False
SEED = 777
SAVE_DIR = "./records/train_0520"
EVENTS_COUNT = 1200
MAX_EPOCH = 24
LOAD_MODEL = False
TRAIN_SAMPLE_COUNT = 1000 if DEBUG else 5000

# Hyperparameters
CRITERIA = (0.02, 0.02) if DEBUG else (0.002, 0.000)
PARAMS = {
    "input_shape": (2, 36, 36),  # "channel", "height", "width"
    "conv1": {
        "out_channels": 20,
        "kernel_size": 5,
        "spike_threshold": 10,
        "lr_potentiation": 0.4,
        "lr_depression": 0.05,
        "min_weight": -0.1,
        "convergence_criteria": CRITERIA[0],
        "decay_rate": 1 - 1e-4,
    },
    "pool1": {"kernel_size": 2, "stride": 2, "spike_threshold": 1},
    "conv2": {
        "out_channels": 40,
        "kernel_size": 5,
        "spike_threshold": 10,
        "lr_potentiation": 0.4,
        "lr_depression": 0.05,
        "min_weight": -0.1,
        "convergence_criteria": CRITERIA[1],
        "decay_rate": 1 - 1e-4,
    },
    "pool2": {"kernel_size": 2, "stride": 2, "spike_threshold": 1},
    "linear": {
        "out_channels": 300,
        "spike_threshold": 10,
        "lr_potentiation": 0.4,
        "lr_depression": 0.05,
        "min_weight": -0.1,
        "decay_rate": 1 - 1e-4,
    },
}


np.random.seed(SEED)
torch.manual_seed(SEED)


def display_params():
    print("Hyperparameters:")
    for k, v in PARAMS.items():
        print(f"{k}: {v}")
    print(f"Save directory: {SAVE_DIR}")
    print(f"Events count: {EVENTS_COUNT}")
    print(f"Max epoch: {MAX_EPOCH}")
    print(f"Debug mode: {DEBUG}")
    print(f"Load model: {LOAD_MODEL}")
    print(f"Seed: {SEED}")
    print(f"Convergence criteria: {CRITERIA}")
    print("\n")
    confirm = input("Continue? (y/n): ")
    if confirm.lower() != "y":
        exit(0)


def write_hyperparameters(dir_name: str = SAVE_DIR):
    with open(os.path.join(dir_name, "hyperparameters.yaml"), "w") as f:
        yaml.dump(PARAMS, f)


def extract_events(ds: NMNIST, event_count: int):
    ds = tqdm(ds)
    ds.set_description(f"Extracting first {event_count} events")
    return [(e[:event_count], label) for e, label in ds]


def event_to_spike_sample(events: np.ndarray) -> List[Spike2D]:
    return [
        Spike2D(x=event["x"], y=event["y"], channel=event["p"], time=event["t"])
        for event in events
    ]


def event_to_spike_dataset(dataset: NMNIST) -> List:
    dataset = tqdm(dataset)
    dataset.set_description("Converting events to spikes")
    return [(event_to_spike_sample(e), label) for e, label in dataset]


def shuffle_dataset(dataset: Iterable, length: int = None) -> List:
    if length:
        shuffle_indice = torch.randperm(len(dataset))[:length]
    else:
        shuffle_indice = torch.randperm(len(dataset))
    return [dataset[i] for i in shuffle_indice]


def pack_dataset(dataset: List[Tuple[List[Spike2D], int]]):
    return DataLoader(dataset, batch_size=1, shuffle=True, num_workers=3)


def build_dataset():
    print(f"Loading NMNIST dataset for training")
    train_ds = NMNIST(save_to="./data", train=True)
    train_ds = shuffle_dataset(train_ds, 1000 if DEBUG else None)
    train_ds = extract_events(train_ds, EVENTS_COUNT)
    train_ds = event_to_spike_dataset(train_ds)
    print(f"Loading NMNIST dataset for testing")
    test_ds = NMNIST(save_to="./data", train=False)
    test_ds = shuffle_dataset(test_ds, 300 if DEBUG else None)
    test_ds = extract_events(test_ds, EVENTS_COUNT)
    test_ds = event_to_spike_dataset(test_ds)
    return train_ds, test_ds


def build_training_network():
    if LOAD_MODEL:
        return torch.load(os.path.join(SAVE_DIR, "model.pth"))
    else:
        conv_layer_1 = Conv2D(input_shape=PARAMS["input_shape"], **PARAMS["conv1"])
        pool_layer_1 = Pool2D(
            input_shape=conv_layer_1.get_output_shape(), **PARAMS["pool1"]
        )
        conv_layer_2 = Conv2D(
            input_shape=pool_layer_1.get_output_shape(), **PARAMS["conv2"]
        )
        pool_layer_2 = Pool2D(
            input_shape=conv_layer_2.get_output_shape(), **PARAMS["pool2"]
        )
        flatten = Flatten(input_shape=pool_layer_2.get_output_shape())
        linear = Linear(in_channels=flatten.get_out_channels(), **PARAMS["linear"])
        torch.save(conv_layer_1.get_weight(), os.path.join(SAVE_DIR, "conv1.pth"))
        torch.save(conv_layer_2.get_weight(), os.path.join(SAVE_DIR, "conv2.pth"))
        return STDPSequential(
            conv_layer_1, pool_layer_1, conv_layer_2, pool_layer_2, flatten, linear
        )


def training_epoch(
    net: STDPSequential,
    train_ds: List[Tuple[List[Spike2D], int]],
    # visualizer: ConvLayerVisualizer,
    epoch: int,
):
    train_ds = shuffle_dataset(train_ds, TRAIN_SAMPLE_COUNT)
    pbar = tqdm(total=len(train_ds))
    pbar.set_description(f"Epoch {epoch+1}")
    log_dir = os.path.join(SAVE_DIR, f"logs/epoch({epoch})")
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    for i, (e, _) in enumerate(train_ds):
        _ = net(e)
        pbar.update(1)
        if i % 100 == 0:
            conv_layer_1: Conv2D = net[0]
            conv_layer_2: Conv2D = net[2]
            linear_layer: Linear = net[5]
            conv_layer_1_score = conv_layer_1.cal_convergence()
            conv_layer_2_score = conv_layer_2.cal_convergence()
            linear_layer_score = linear_layer.cal_convergence()
            pbar.set_postfix_str(
                f"converge score: {conv_layer_1_score:.4f}|{conv_layer_2_score:.4f}|{linear_layer_score:.4f}"
            )
            conv_layer_1_weight = conv_layer_1.get_weight()
            conv_layer_2_weight = conv_layer_2.get_weight()
            linear_layer_weight = linear_layer.get_weight()
            torch.save(conv_layer_1_weight, os.path.join(SAVE_DIR, "conv1.pth"))
            torch.save(conv_layer_2_weight, os.path.join(SAVE_DIR, "conv2.pth"))
            torch.save(linear_layer_weight, os.path.join(SAVE_DIR, "linear.pth"))
        if i % 1000 == 0:
            net.save(os.path.join(SAVE_DIR, "model.pth"))
    conv_layer_1_score = conv_layer_1.cal_convergence()
    conv_layer_2_score = conv_layer_2.cal_convergence()
    linear_layer_score = linear_layer.cal_convergence()
    torch.save(
        conv_layer_1_weight,
        os.path.join(log_dir, f"conv1_{conv_layer_1_score:.4f}.pth"),
    )
    torch.save(
        conv_layer_2_weight,
        os.path.join(log_dir, f"conv2_{conv_layer_2_score:.4f}.pth"),
    )
    torch.save(
        linear_layer_weight,
        os.path.join(log_dir, f"linear_{linear_layer_score:.4f}.pth"),
    )


def is_train_continue(net):
    out = False
    for i, layer in enumerate(net):
        if hasattr(layer, "is_converged"):
            if not layer.is_converged():
                layer.train(True)
                out = True
            else:
                layer.train(False)
    return out


def check_weight_dir(dir_name: str):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    return dir


def save_weights(net: STDPSequential, dir: str):
    weights = net.get_weights()
    torch.save(weights, os.path.join(dir, "weights.pth"))


def run_monitor(path: str):
    monitor = Conv2DWeightMonitor(path)
    monitor.run()


def build_monitor_process():
    path_1 = os.path.join(SAVE_DIR, "conv1.pth")
    path_2 = os.path.join(SAVE_DIR, "conv2.pth")
    p1 = Process(target=run_monitor, args=(path_1,))
    p2 = Process(target=run_monitor, args=(path_2,))
    return p1, p2


def convert_output_to_accumulator(
    net: STDPSequential, events: List[Spike2D], output_shape: Tuple[int, int, int]
):
    out_events: List[Spike2D] = net(events)
    out_channels = output_shape[0]
    out_h = output_shape[1]
    out_w = output_shape[2]
    out_layer: Linear = net[5]
    # accumulators = np.zeros(out_channels * out_h * out_w)
    accumulators = np.zeros(out_layer.get_output_channels())
    for e in out_events:
        accumulators[e.channel] += 1
    return accumulators / accumulators.sum()


def convert_dataset_to_events(
    dataset: tqdm, net: STDPSequential, output_shape: Tuple[int, int, int]
):
    X = []
    Y = []
    for events, label in dataset:
        for layer in net:
            if isinstance(layer, Conv2D):
                layer.reset_potential()
        accumulators = convert_output_to_accumulator(net, events, output_shape)
        X.append(accumulators)
        Y.append(label)
    return np.array(X), np.array(Y)


def fit_and_score_SVM(X_train, Y_train, X_test, Y_test) -> float:
    clf = SVC(C=1)
    clf.fit(X_train, Y_train)
    score = clf.score(X_test, Y_test)
    return score


def evaluate(
    net: STDPSequential,
    train_ds: List[Tuple[List[Spike2D], int]],
    test_ds: List[Tuple[List[Spike2D], int]],
) -> float:
    print("Evaluating network performance")
    p_bar = tqdm(train_ds[:1000])
    p_bar.set_description_str(f"Convert training dataset to network output")
    output_shape = net[3].get_output_shape()
    X_train, Y_train = convert_dataset_to_events(p_bar, net, output_shape)
    p_bar = tqdm(test_ds[:300])
    p_bar.set_description_str(f"Convert testing dataset to network output")
    X_test, Y_test = convert_dataset_to_events(p_bar, net, output_shape)
    print("Fitting SVM model with network output...")
    score = fit_and_score_SVM(X_train, Y_train, X_test, Y_test)
    print(f"SVM Score: {score:.4f}")
    return score


def evaluate_and_save(
    net: STDPSequential,
    save_dir: str,
    train_ds: List[Tuple[List[Spike2D], int]],
    test_ds: List[Tuple[List[Spike2D], int]],
    max_score: float,
):
    score = evaluate(net, train_ds, test_ds)
    if score > max_score:
        if os.path.exists(os.path.join(save_dir, f"best_model_{max_score:.4f}.pth")):
            os.remove(os.path.join(save_dir, f"best_model_{max_score:.4f}.pth"))
        max_score = score
        net.save(os.path.join(save_dir, f"best_model_{max_score:.4f}.pth"))
        print(f"New best model saved with score: {max_score:.4f}")
    return max_score


def is_train(net: STDPSequential):
    for layer in net:
        if isinstance(layer, Conv2D):
            layer.train(not layer.is_converged())
    return


def disable_training(net: STDPSequential):
    for layer in net:
        if isinstance(layer, Conv2D):
            layer.train(False)
    return


def training_loop(
    net: STDPSequential,
    train_ds,
    test_ds,
    epochs: int,
    save_dir: str = None,
):
    if save_dir:
        check_weight_dir(save_dir)

    # try:
    max_score = 0
    for epoch in range(epochs):
        is_train(net)
        if not is_train_continue(net):
            print("All layers are converged")
            break
        training_epoch(net, train_ds=train_ds, epoch=epoch)
        for i, layer in enumerate(net):
            if isinstance(layer, Conv2D):
                print(f"layer[{i}] converge score: {layer.cal_convergence():.4f}")
        if save_dir:
            disable_training(net)
            net.save(os.path.join(save_dir, "latest_model.pth"))
            max_score = evaluate_and_save(net, save_dir, train_ds, test_ds, max_score)
        print("\n")
    _ = input()
    return


def main():
    display_params()
    check_weight_dir(dir_name=SAVE_DIR)
    write_hyperparameters(dir_name=SAVE_DIR)
    train_ds, test_ds = build_dataset()
    net = build_training_network()
    monitor_process_1, monitor_process_2 = build_monitor_process()
    monitor_process_1.start()
    monitor_process_2.start()
    training_loop(net, train_ds, test_ds, MAX_EPOCH, SAVE_DIR)
    monitor_process_1.join()
    monitor_process_2.join()


if __name__ == "__main__":
    main()
